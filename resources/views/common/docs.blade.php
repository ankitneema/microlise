@if(!$userDocs->isEmpty())
	@foreach($userDocs as $doc)
	<div class="col-sm-4">
		<div class="thumbnail">
		  <a href="#" class="">
		    <img style="width: 100%;height: 200px" src="{{ asset('storage/categoryImages/'.$doc->file_name) }}" class="">
		   </a>
		</div>
	</div>
	@endforeach
@else
	<p>No Document Uploaded</p>
@endif