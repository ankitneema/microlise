@if ($message = Session::get('success'))
    <div class="alert alert-success custom-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <i class="fa fa-check fa-lg" aria-hidden="true"></i> <strong>Success!</strong> {{ $message }}
    </div>
@endif
@if ($message = Session::get('error'))
    <div class="alert alert-danger custom-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> {{ $message }}
    </div>
@endif