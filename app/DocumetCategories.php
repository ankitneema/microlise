<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class DocumetCategories extends Model
{
    protected  $table     = "document_categories";
    protected  $guarded   = ['id'];
}