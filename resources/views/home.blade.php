@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                    <div class="card-body">
                       
                        @include('common/errors')
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('uploadFiles')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">Category</label>
                                <div class="col-md-6">
                                  <select name="document_category" class="form-control">
                                        <option value="">Select Category</option>
                                        @if(!$documetCategory->isEmpty())
                                            @foreach($documetCategory as $category)
                                                <option value="{{ $category->id}}">{{ $category->name}}</option>
                                            @endforeach
                                        @endif
                                  </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">File</label>
                                <div class="col-md-6">
                                    <input type="file" id="file" class="form-control" name="file" required>
                                </div>
                            </div>

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </form>
                    </div>

                    <div class="card-body">
                       
                        <ul class="nav nav-tabs" role="tablist">
                            @if(!$documetCategory->isEmpty())
                                @foreach($documetCategory as $key => $category)
                                    <li class="nav-item">
                                      <a class="nav-link {{ $key ==0?'active':'' }}" data-toggle="tab" href="#{{$category->name}}" dataToShow="{{$category->id}}" >{{ $category->name}}</a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>

                        <div class="tab-content">
                            @if(!$documetCategory->isEmpty())
                                @foreach($documetCategory as $keyContent => $categoryContent)
                                    <div id="{{ $categoryContent->name}}" class="container tab-pane {{ $keyContent ==0?'active':'' }}"><br>
                                       
                                        <div class="row appendData" id="appendData">
                                            
                                            
                                        </div><!--/row -->
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@push('js_scripts')
<script type="text/javascript">
$(document).ready(function(){
    activaTab(1);
    $('ul.nav-tabs').on("click", "li", function (event) {  
        var categoryId = $(this).find('a').attr('dataToShow');
        activaTab(categoryId);
    });
});

function activaTab(categoryId){
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:"{{route('userDocuments')}}",
        type:"POST",
        data: {'categoryId':categoryId},
        beforeSend:function(){
            $('.appendData').empty();
        },   
        success:function(data)
        {
            console.log
            $(".appendData").append(data);
        },
        error: function(e)
        {  }
    });
};
</script>
@endpush
@endsection
