<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [    
                "id"	=>1,
                "name"	=>"Ankit",
                "email"	=>"ankit.neema@gmail.com",
                "email_verified_at" =>null,
                "password" => bcrypt('12345678'),
                "remember_token"=>null,
                "created_at"    => \Carbon\Carbon::now(),
                "updated_at"    => \Carbon\Carbon::now(),
            ],
            [    
                "id"    =>1,
                "name"  =>"Ankit",
                "email" =>"ankit@gmail.com",
                "email_verified_at" =>null,
                "password" => bcrypt('12345678'),
                "remember_token"=>null,
                "created_at"    => \Carbon\Carbon::now(),
                "updated_at"    => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
