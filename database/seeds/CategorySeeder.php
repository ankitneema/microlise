<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_categories')->insert([
            [    
                "id"	=>1,
                "name"	=>"Image",
                "status"=>1,
                "created_at"    => \Carbon\Carbon::now(),
                "updated_at"    => \Carbon\Carbon::now(),
            ], [    
                "id"    =>2,
                "name"  =>"Video",
                "status"=>1,
                "created_at"    => \Carbon\Carbon::now(),
                "updated_at"    => \Carbon\Carbon::now(),
            ], [    
                "id"    =>3,
                "name"  =>"Documents",
                "status"=>1,
                "created_at"    => \Carbon\Carbon::now(),
                "updated_at"    => \Carbon\Carbon::now(),
            ],[    
                "id"    =>4,
                "name"  =>"Others",
                "status"=>1,
                "created_at"    => \Carbon\Carbon::now(),
                "updated_at"    => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
