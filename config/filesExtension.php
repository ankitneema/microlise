<?php

return [

   "image" => ['image/jpeg','image/png'],
   "video" => ['video/mp4'],
   "pdf"   => ['application/pdf'],
   "audio" => ['audio/mpeg']
];