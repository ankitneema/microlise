<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\DocumetCategories;
use App\UserDocument;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try{
            $documetCategory = DocumetCategories::where('status',1)->get();
            return view('home',["documetCategory"=>$documetCategory]);
        }catch(\Exception $e){

        }
    }

    public function uploadFiles(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                "file"              =>'mimetypes:image/jpeg,image/png,video/mp4,application/pdf,audio/mpeg',
                "document_category"  =>'required',
            ]);

            if($validator->fails()){
                return redirect(route('home'))->withErrors($validator);
            }else{

                if ($request->hasFile('file')) {
                    
                    $image  = $request->file('file');
                    $name   = time().$image->getClientOriginalName();
                    $destinationPath = public_path('/storage/categoryImages/');
                    $image->move($destinationPath, $name);

                    $user = \Auth::user();
                    $userDocs = UserDocument::create([
                        'user_id' => $user->id,
                        'document_category_id' => $request->document_category,
                        'file_name' =>$name
                    ]);
                    if(!empty($userDocs)){
                        return redirect(route('home'))->with('success', 'File uploaded successfully.');
                    }else{
                        return redirect(route('home'))->with("error","Something Wrong, Unable to store data.");
                    }
                }else{
                    return redirect(route('home'))->with("error","Something Wrong, Please try later.");
                }
            }
        }catch(\Exception $e){
            \Log::error($e);
            return redirect(route('home'))->with("error","Unable to store user documents, Please try later.");
        }
    }

    public function userDocuments(Request $request){
        try{
            if(!empty($request->categoryId)){
                $userDocs = UserDocument::where('user_id',\Auth::user()->id)->where('document_category_id',$request->categoryId)->get();
                return view('common.docs',['userDocs'=>$userDocs]);
            }
        }catch(\Exception $e){
            \Log::error($e);
            return redirect(route('home'))->with("error","Unable to store user documents, Please try later.");
        }
    }
}